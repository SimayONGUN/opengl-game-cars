/////////////////////////////////////////////////////////         
// assg1.cpp
//
// This program draws a red diamond, a blue circle and a color-changing triangle and moves them by mouse and keyboard inputs.
//
// 
///////////////////////////////////////////////////////// 

#include <cmath>
#include <iostream>

#ifdef __APPLE__
#  include <GLUT/glut.h>
#else
#  include <GL/glut.h>
#endif

#define PI 3.14159265

using namespace std;

// Globals.
static GLsizei width, height; // OpenGL window size.
static float windowWidth = 400;
static float windowHeight = 400;

							  // Drawing routine.
static int line_segments = 100;         //number of line segments of the circle
static float Xcentre_of_the_circle = 0; //x coordinate of the center of the circle
static float Ycentre_of_the_circle = 0; //y coordinate of the center of the circle
static float color_of_the_triangle = 0; //if it is 1, then the triangle is black. if it is 0 the triangle is green
static int leftMouseButtonClicked = 0;  //if left mouse button is clicked, this variable is not equals to 0 mod2.
static int rightButtonClicked = 0;      //if right mouse button is clicked, this variable is not equals to 0 mod2.
static float xLofDiamond = -1.5;        //x coordinate of left edge of the diamond
static float yLofDiamond = 0;           //y coordinate of left edge of the diamond
static float xUofDiamond = 0;           //x coordinate of top of the diamond
static float yUofDiamond = 2;           //y coordinate of top edge of the diamond
static float xRofDiamond = 1.5;         //x coordinate of righ edge of the diamond
static float yRofDiamond = 0;           //y coordinate of righ edge of the diamond
static float xDofDiamond = 0;           //x coordinate of bottom edge of the diamond
static float yDofDiamond = -2;          //y coordinate of bottom edge of the diamond

void drawScene(void)
{
	glClear(GL_COLOR_BUFFER_BIT);

	// Define the viewport.
	glViewport(0, 0, width, height);

	// Begin contents of first viewport.
	if (leftMouseButtonClicked % 2 == 0) {  //if left button of the mouse is clicked
		//the red diamond
		glColor3f(1.0, 0.0, 0.0);
		glBegin(GL_POLYGON);
		glVertex2f(xLofDiamond, yLofDiamond);
		glVertex2f(xUofDiamond, yUofDiamond);
		glVertex2f(xRofDiamond, yRofDiamond);
		glVertex2f(xDofDiamond, yDofDiamond);
		glEnd();

		glColor3f(0.0, 0.0, 1.0);
		glBegin(GL_TRIANGLE_FAN);
		// the blue circle.
		glVertex2f(Xcentre_of_the_circle, Ycentre_of_the_circle);

		float angle;
		int i;

		//glBegin(GL_LINE_LOOP);
		for (i = 0; i < line_segments; ++i)
		{
			angle = 2 * PI * i / line_segments;
			glVertex2f(Xcentre_of_the_circle + cos(angle) * 1.5, Ycentre_of_the_circle + sin(angle) * 1.5);
		}

		glVertex2f(Xcentre_of_the_circle + cos(2 * PI * i / line_segments) * 1.5, Ycentre_of_the_circle + sin(2 * PI * i / line_segments) * 1.5);
		glEnd();
	}

	else { //if left button of the mouse is not clicked

		//the blue circle
		glColor3f(0.0, 0.0, 1.0);
		glBegin(GL_TRIANGLE_FAN);
		// A blue circle.
		glVertex2f(Xcentre_of_the_circle, Ycentre_of_the_circle);

		float angle;
		int i;

		//glBegin(GL_LINE_LOOP);
		for (i = 0; i < line_segments; ++i)
		{
			angle = 2 * PI * i / line_segments;
			glVertex2f(Xcentre_of_the_circle + cos(angle) * 1.5, Ycentre_of_the_circle + sin(angle) * 1.5);
		}

		glVertex2f(Xcentre_of_the_circle + cos(2 * PI * i / line_segments) * 1.5, Ycentre_of_the_circle + sin(2 * PI * i / line_segments) * 1.5);
		glEnd();


		//the red diamond
		glColor3f(1.0, 0.0, 0.0);
		glBegin(GL_POLYGON);
		glVertex2f(xLofDiamond, yLofDiamond);
		glVertex2f(xUofDiamond, yUofDiamond);
		glVertex2f(xRofDiamond, yRofDiamond);
		glVertex2f(xDofDiamond, yDofDiamond);
		glEnd();
	}

	
	//the color-changing triangle
	glColor3f(0,color_of_the_triangle,0);
	glBegin(GL_POLYGON);
	float squareRootOfThree = 1.732;
	glVertex2f(-0.8,(-0.8/3*squareRootOfThree));
	glVertex2f(0.8, (-0.8 / 3 * squareRootOfThree));
	glVertex2f(0, 2*(0.8 / 3 * squareRootOfThree));
	glEnd();
	

	// End contents of viewport.
	glFlush();

}

// Initialization routine.
void setup(void)
{
	glClearColor(1.0, 1.0, 1.0, 0.0);
}

// OpenGL window reshape routine.
void resize(int w, int h)  // if the size of the window is changed, change the size of the shapes
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-4, +4, -4, +4, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Pass the size of the OpenGL window to globals.
	width = w;
	height = h;
	windowWidth = glutGet(GLUT_WINDOW_WIDTH);
	windowHeight = glutGet(GLUT_WINDOW_HEIGHT);
}

// Keyboard input processing routine.

void keyInput(unsigned char key, int x, int y) // keyboard inputs
{
	switch (key)
	{
	case 'q':  //if q is pressed quit
		exit(0);
		break;
	case 'c': // if c is pressed, change the resolution of the circle
		if (line_segments != 100)
			line_segments = 100;
		else
			line_segments = 20;
		glutPostRedisplay();
		break;
	
	default:
		break;
	}
}

void specialKeyInput(int key, int x, int y)   // move the diamond if shift + arrow keys are pressed
{

	if (key == GLUT_KEY_RIGHT)  {  // if the right arrow button is pressed
		if (glutGetModifiers() & GLUT_ACTIVE_SHIFT) { //if shift is pressed
			xLofDiamond += 0.1;
			xRofDiamond += 0.1;
			xDofDiamond += 0.1;
			xUofDiamond += 0.1;
			
		}
		else {         // if shift is not pressed
			if (Xcentre_of_the_circle < 4)
				Xcentre_of_the_circle += 0.1;
		}
		
		

	}
	if (key == GLUT_KEY_LEFT) {// if the left arrow button is pressed
		if (glutGetModifiers() & GLUT_ACTIVE_SHIFT) {  //if shift is pressed
			xLofDiamond -= 0.1;
			xRofDiamond -= 0.1;
			xDofDiamond -= 0.1;
			xUofDiamond -= 0.1;

		}
		else {             //if shift is not pressed
			if (Xcentre_of_the_circle > -4)
				Xcentre_of_the_circle -= 0.1;
		}
	}

	if (key == GLUT_KEY_UP)  {// if the up arrow button is pressed
		if (glutGetModifiers() & GLUT_ACTIVE_SHIFT) {  //if shift is pressed
			yLofDiamond += 0.1;
			yRofDiamond += 0.1;
			yDofDiamond += 0.1;
			yUofDiamond += 0.1;

		}
		else {       //if shift is not pressed
			if (Ycentre_of_the_circle < 4)
				Ycentre_of_the_circle += 0.1;
		}
	}

	if (key == GLUT_KEY_DOWN) {// if the down arrow button is pressed
		if (glutGetModifiers() & GLUT_ACTIVE_SHIFT) {  // if shift is pressed 
			yLofDiamond -= 0.1;
			yRofDiamond -= 0.1;
			yDofDiamond -= 0.1;
			yUofDiamond -= 0.1;

		}
		else {        // if shift is not pressed
			if (Ycentre_of_the_circle > -4)
				Ycentre_of_the_circle -= 0.1;
		}
		}
	


	glutPostRedisplay();
}


void myTimerFnc(int a) {       // change the color of the triangle every 15 seconds

	glutTimerFunc(15000, myTimerFnc, 0);
	if (color_of_the_triangle != 0)
		color_of_the_triangle = 0;
	else
		color_of_the_triangle = 1;

	glutPostRedisplay();

}


void mouse(int button, int state, int x, int y)  // increase the variable that related with the clicked button of the mouse 
{
	switch (button) {
	case GLUT_LEFT_BUTTON: {
		leftMouseButtonClicked++;
		break;
			}
	case GLUT_RIGHT_BUTTON: {
		rightButtonClicked++;
		break;
	}
		}
	glutPostRedisplay();
	}

void myMotionFunc(int x, int y)  //mouse motion function
{
	if (rightButtonClicked%2==1) {            //right button of the mouse is clicked
		xLofDiamond = (((x / windowWidth) * 8) - 4) - 1.5;
		yLofDiamond = (((y / windowHeight)*(-8)) + 4);
		xUofDiamond = ((x / windowWidth) * 8) - 4;
		yUofDiamond = (((y / windowHeight)*(-8)) + 4) + 2;
		xRofDiamond = ((x / windowWidth) * 8) - 4 + 1.5;
		yRofDiamond = ((y / windowHeight)*(-8)) + 4;
		xDofDiamond = ((x / windowWidth) * 8) - 4;
		yDofDiamond = ((y / windowHeight)*(-8)) + 4- 2;
	}
	else {                                     //left button of the mouse is clicked

		Xcentre_of_the_circle = ((x / windowWidth) * 8) - 4;
		Ycentre_of_the_circle = ((y / windowHeight)*(-8)) + 4;
		if (Xcentre_of_the_circle > 4)       // dont let the centre of the circle be out of the application window
			Xcentre_of_the_circle = 4;
		else if (Xcentre_of_the_circle < -4) // dont let the centre of the circle be out of the application window
			Xcentre_of_the_circle = -4;
		if (Ycentre_of_the_circle > 4)       // dont let the centre of the circle be out of the application window
			Ycentre_of_the_circle = 4;
		else if (Ycentre_of_the_circle < -4) // dont let the centre of the circle be out of the application window
			Ycentre_of_the_circle = -4;

	}
	
	
	glutPostRedisplay();
}




// Main routine.
int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(windowWidth, windowHeight);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Assg1");
	setup();
	glutDisplayFunc(drawScene);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyInput);
	glutMouseFunc(mouse);
	glutSpecialFunc(specialKeyInput);
	glutMotionFunc(myMotionFunc);
	myTimerFnc(0);
	glutMainLoop();

	return 0;
}

